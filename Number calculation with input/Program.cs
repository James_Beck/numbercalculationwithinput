﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Number_calculation_with_input
{
    class Program
    {
        static void Main(string[] args)
        {
            ///Store of variables
            double a = 0.00;
            double b = 0.00;
            double c = 0.00;
            double d = 0.00;
            double e = 0.00;

            ///Beginning of interaction user
            Console.WriteLine("Ok, so help me out. I'm making an equation like so [(a + b) * c / (d - e)]");
            Console.WriteLine("What I need you to do is, when prompted, input the values for variables a, b, c, d, and e.");
            Console.WriteLine("So, in their respective alphabetical order, enter any five numbers for the variables");
            ///Code to interact with user and request the input of the variables
            a = double.Parse(Console.ReadLine());
            b = double.Parse(Console.ReadLine());
            c = double.Parse(Console.ReadLine());
            d = double.Parse(Console.ReadLine());
            e = double.Parse(Console.ReadLine());
            ///The calculation of the numbers that the user has given
            Console.WriteLine($"Great, I think you'll find that equals {(a + b) * c / (d - e)}");
            
        }
    }
}
